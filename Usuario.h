/*
 * Jugador.h
 *
 *  Created on: 15/05/2018
 *      Author: patron
 */

#ifndef USUARIO_H_
#define USUARIO_H_

#include "Lista.h"
#include "CondicionesIniciales.h"
#include "Terreno.h"

#include "EasyBMP.h"

//luego borrar.
#include <iostream>

#include "Almacen.h"


/* Clase encargada de administrar las modificaciones
 * inherentes a cada uno de los jugadores segun las acciones
 *  que realice sobre cada uno de sus terrenos a lo largo del juego.
 */
class Usuario{

	private:
		Lista<Terreno*>* terrenos;
		ui creditosActuales;
		ui unidadesRiegoActuales;
		ui capacidadTanqueActual;
		ui numJugador; // ultizado al momento de declarar ganador/es. El numero de jugador permanece fijo durante el juego.
		Almacen* almacen;



	public:

		/* Constructor sin parámetros.
		 * Incialmente el usuario queda:
		 *
		 * 1.Lista de terrenos vacia
		 * 2.creditos iniciales nulo.
		 * 3.unidadesRiego nulas.
		 * 4.capacidadTanqueActual nula.
		 * 5.Instancia la almacen donde se almacenaran
		 *  las cosechas que se realicen.
		 */
		Usuario();

		/* Pre: parametrosIniciales fue creado.
		 * Post: carga todos los atributos correspondientes a un jugador.
		 * deja al jugador listo para comenzar el juego.
		 * segun las Condiciones elegidas(Ver Manual programador).
		 */
		void cargarParametrosIniciales(CondicionesIniciales* parametrosIniciales, ui numJugador);

		/*
		 * Post: imprimio los datos del estado actual del usuario.
		 * Es decir, sus atributos disponibles en el juego.
		 * por ejemplo:
		 * cantidad de creditos, unidades de riego, ect.
		 */
		void imprimirEstadoJugador();

		/*
		 * Post: creo el archivo bmp del terreno, sobre el directorio sobre el cual se ejecuta el juego,
		 * indicandose de la forma:
		 *
		 * "jugador_<Numero>_Terreno<Numero>_Turno<Numero>.bmp"
		 *
		 * Nota: Ver referencias de terreno en manual de usuario.
		 */
		void imprimirEstadoTerrenos(ui numJugador, ui cantTurnosAct, CondicionesIniciales* condIniciales);

		/* Post: Devuelve un numero [1..6],
		 * resultado de lasimulacion del tirado de un dado.
		 */
		ui tirarDado();

		/*
		 * Post: Devuelve la cantidad de unidades de riego disponibles.
		 * para el turno actual del jugador.
		 */
		ui calcularUnidadesRiego(ui dado);

		/***** métodos de obtención de parámetros *****/

		/*
		 * Post: Devolvio la lista de terrenos.
		 */
		Lista<Terreno*>* obtenerTerrenos();

		/*
		 * Post: Devuelve el almacen de cosechas del usuario.
		 */
		Almacen* obtenerAlmacen();

		/*
		 * Post:Devuelve la cantidad de unidades de riego
		 *  que puede almacenar el usuario.
		 */
		ui verCapacidadTanqueActual();

		/*
		 * Post: Devuelve la cantidad de unidades de riego disponibles
		 * para su uso.
		 */
		ui verUnidadesRiegoActuales();

		/*
		 * Post: Devuelve la cantidadde creditos disponibles
		 * para ejecutar las distintas acciones de juego
		 */
		ui verCreditosActuales();

		/*
		 * Post: Devuelve la cantidad de cosechas
		 * posibles de guardar en el almacen.
		 */
		ui verCapacidadAlmacen();

		/*
		 * Post: Devuelve la cantidad de terrenos que posee el usuario.
		 */
		ui contarTerrenos();

		/*
		 * Post: Devuelve el numero de jugador asignado al momento de iniciar el juego.
		 * Devuelve 0, si no fue asignado.
		 */
		ui obtenerNumJugador();

		/***** Metodos de modificacion del juego *****/

		/*
		 * Post: el usuario cuenta con un nuevo terreno para accionar.
		 * Se le asignará un numero de terreno para identificarlo.
		 * 2, si es el primer terreno que compra
		 * 3, si es el segundo ...., asi sucesivamente.
		 */
		void agregarTerreno(ui alto, ui ancho);

		/*
		 * Post: Cambio la capacidad de unidades de riego actual por CapacidadNueva.
		 */
		void cambiarCapacidadTanqueActual(ui CapacidadNueva);

		/*
		 * Post: Cambio las unidades de riego con que cuenta el usuario.
		 */
		void setUnidadesDeRiego(ui unidadesDeRiegoNueva);

		/* Pre: valorDeIncremento es un entero positivo.
		 * Post: Incrementa los creditos del usuario segun el valorDeIncremento
		 */
		void aumentarCreditos(ui valorDeIncremento);

		/*
		 * Post: Reduce los creditos del usuario segun el valorDeReduccion.
		 */
		void reducirCreditos(ui valorDeReduccion);

		/*
		 * Post: Incremento las unidades de riego segun valorDeIncremento.
		 */
		void aumentarUnidadesRiego(ui valorDeIncremento);

		/*
		 * Post: Reduce las unidades de riego segun el valorDeReduccion.
		 */
		void reducirUnidadesRiego(ui valorDeReduccion);

		/*
		 * Post: se modifico la capacidad dealmacen de cosechas segun CapacidadNueva.
		 */
		void cambiarCapacidadAlmacen(ui CapacidadNueva);

		/***** Metodos de liberacion de memoria *****/

		/*
		 * Post: libera memoria solicitada sobre cada uno de los terrenos del usuario.
		 * almacenados sobre una lista simplemente enlazada.
		 */
		void liberarTerrenos();

		/*
		 * Post: se libero la memoria solicitada.
		 */
		~Usuario();

};

#endif /* JUGADOR_H_ */
