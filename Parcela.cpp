/*
 * Parcela.cpp
 *
 *  Created on: 20 may. 2018
 *      Author: patron
 */

#include "Parcela.h"
#include <string>
#include <iostream>

Parcela::Parcela() {
	this->ocupada = false;
	this->regada = false;
	this->recuperandose=false;
	this->tipoDeCultivo = NULL;
	this->tiempoDeCosecha = 0;
	this->tiempoDeRecuperacion = 0;
}

/******* Getters ******/

bool Parcela::estaOcupada(){
	return this->ocupada;
}

bool Parcela::estaRegada(){
	return this->regada;
}

bool Parcela::enRecuperacion(){
	return this->recuperandose;
}

ui Parcela::verTiempoDeCosecha(){
	return this->tiempoDeCosecha;
}

ui Parcela::verTiempoDeRecuperacion(){
	return this->tiempoDeRecuperacion;
}

Cultivo* Parcela::getCultivo(){
	return this->tipoDeCultivo;
}

/******* Setters ********/

void Parcela::setCultivo(Cultivo* cultivo){
	this->tipoDeCultivo = cultivo;
}

void Parcela::setTiempoDeCosecha(ui tiempoDeCosecha){
	this->tiempoDeCosecha = tiempoDeCosecha;
}

void Parcela::setTiempoDeRecuperacion(ui tiempoDeRecuperacion){
	this->tiempoDeRecuperacion = tiempoDeRecuperacion;
}

void Parcela::setParcelaRegada(){
	this->regada = true;
}

void Parcela::setParcelaSinRegar(){
	this->regada=false;
}

void Parcela::setParcelaLibre(){
	this->ocupada = false;
}

void Parcela::setParcelaOcupada(){
	this->ocupada = true;
}

void Parcela::setParcelaEnRecuperacion(){
	this->recuperandose = true;
}

void Parcela::setParcelaRecuperada(){
	this->recuperandose = false;
}

void Parcela::imprimirEstado(){
	bool fueRegada = this->estaRegada();
	bool recuperandose = this->enRecuperacion();
	ui tiempoDeCosecha = this->verTiempoDeCosecha();
	ui tiempoDeRecup = this->verTiempoDeRecuperacion();
	std::cout << "Cultivo Analizado:" << this->getCultivo()->verNombre() << std::endl;
	if(fueRegada){
		std::cout << "El Cultivo fue Regado" << std::endl;
	}
	else{
		std::cout << "El Cultivo NO fue Regado" << std::endl;
	}
	if(recuperandose){
		std::cout << "El Cultivo esta Recuperandose/Seco" << std::endl;
	}
	else{
		std::cout << "El Cultivo Se encuentra Recuperado" << std::endl;
	}
	std::cout << "TiempoDeCosecha: " << tiempoDeCosecha << std::endl;
	std::cout << "Tmpo De Recup: " << tiempoDeRecup << std::endl;

}

