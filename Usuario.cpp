/*
 * Usuario.cpp
 *
 *  Created on: 15/05/2018
 *      Author: patron
 */

#include "Almacen.h"
#include <cstdlib>
#include "EasyBMP.h"

#include <iostream>
#include "Usuario.h"
#include <sstream>
#include <ctime>

#include <string>

using namespace std;

/******** Privadas ********/

ui const ANCHO = 640;     // ANCHO del mapa
ui const ALTO = 480;     // ALTO  del mapa
ui const DEPTH = 24;      // Profundidad del color
ui const SEPARACION_ALTO = 10;
ui const SEPARACION_ANCHO = 10;

ui const MARGIN_LEFT = 120;
ui const MARGIN_TOP = 60;

// Suponiendo que todas las imÃƒÂ¡genes utilizadas para la parcela tienen ese tamanio predefinido
ui const TAMANIO_PARCELA = 50; // 50x50 px;

// Recibe el nombre de una parcela actual.
// Devuelve el estado la imagen representativa del cultivo correspondiente la posicion de la granja.
void obtenerImgEstadoParcela(BMP* parcela, Parcela parcelaAct) {
	if (!parcelaAct.estaOcupada()) {
		parcela->ReadFromFile("img-cultivos/parcela-libre.bmp");
	}
	else if (parcelaAct.enRecuperacion()){
		parcela->ReadFromFile("img-cultivos/parcela-seca.bmp");
	}
	else{
		Cultivo* cultivo = parcelaAct.getCultivo();
		std::string nomCultivo = cultivo->verNombre();
		cout << "nombre del cultivo en parcela ocup: " << nomCultivo << endl;
		if (nomCultivo.compare("frutilla") == 0) {
			parcela->ReadFromFile("img-cultivos/cultivo-frutilla.bmp");
		} else if (nomCultivo.compare("kiwi") == 0) {
			parcela->ReadFromFile("img-cultivos/cultivo-kiwi.bmp");
		} else if (nomCultivo.compare("lechuga") == 0) {
			parcela->ReadFromFile("img-cultivos/cultivo-lechuga.bmp");
		} else if (nomCultivo.compare("papa") == 0) {
			parcela->ReadFromFile("img-cultivos/cultivo-papa.bmp");
		}
	}
}

void mostrarEstadoTerreno(Terreno* terreno, string nombre) {
	Parcela** terrenoAct = terreno->verTerreno();

	// Se crea un BMP
	BMP mapa;
	// Se setean las medidas
	mapa.SetSize(ANCHO, ALTO);
	// Se setea la profundidad del color (Variedad de colores)
	mapa.SetBitDepth(DEPTH);

	unsigned int i = 0; // col mapa (Ver index mapa manual bmp)
	unsigned int j = 0; //fil mapa (ver index mapa manual bmp)

	//Preparo el Fondo de terreno.
	BMP backround;
	backround.ReadFromFile("img-cultivos/granja-background.bmp");

	//Copia el fondo de pantalla del mapa de alguna otra imagen, en este caso backround -> mapa.
	RangedPixelToPixelCopy(backround, 0, backround.TellWidth() - 1,
			backround.TellHeight() - 1, 0, mapa, i, j);

	// el nombre deberÃƒÂ­a cambiar y ser de la forma "nomJugador_TerrenoNro_TurnoNro"

	i = MARGIN_LEFT;
	j = MARGIN_TOP;

	//BMP imgParcela;

	for (unsigned int fil = 0; fil < terreno->verAlto(); fil++) {
		i = 120;
		for (unsigned int col = 0; col < terreno->verAncho(); col++) {
			BMP imgParcela;
			obtenerImgEstadoParcela(&imgParcela, terrenoAct[fil][col]);
			RangedPixelToPixelCopy(imgParcela, 0, TAMANIO_PARCELA - 1,
					TAMANIO_PARCELA - 1, 0, mapa, i, j);
			i += TAMANIO_PARCELA + SEPARACION_ANCHO;
		}
		j += TAMANIO_PARCELA + SEPARACION_ALTO;
	}

	std::cout << "Generando archivo granjeros, por favor espere..."
			<< std::endl;

	// se guarda en el archivo
	mapa.WriteToFile(nombre.c_str());

}

/******** Publicas ********/

Usuario::Usuario() {
	this->terrenos = new Lista<Terreno*>;
	this->almacen = new Almacen();
	this->creditosActuales = 0;
	this->unidadesRiegoActuales = 0;
	this->capacidadTanqueActual = 0;
	this->numJugador = 0;
}

void Usuario::cargarParametrosIniciales(CondicionesIniciales* parametrosIniciales, ui nuevoNumJugador) {
	agregarTerreno(parametrosIniciales->obtenerAltoTerreno(),parametrosIniciales->obtenerAnchoTerreno());
	this->creditosActuales = parametrosIniciales->obtenerCreditos();
	this->unidadesRiegoActuales = UNIDADES_RIEGO_INICIALES; // Inicialmente vacio (0 unidades de riego).
	this->capacidadTanqueActual = parametrosIniciales->obtenerCapacidadTanque();
	this->numJugador = nuevoNumJugador;
	this->almacen->setCapacidad(parametrosIniciales->obtenerCapacidadAlmacen());
}

void Usuario::agregarTerreno(ui alto, ui ancho) {
	Terreno* terreno = new Terreno(alto, ancho);
	this->terrenos->agregar(terreno);

}

ui Usuario::tirarDado() {
	srand(time(NULL));
	ui dado = rand() % 10;
	while (dado > 6 || dado == 0) {
		
		dado = rand() % 10;
	}
	return dado;
}


ui Usuario::calcularUnidadesRiego(ui dado) {
	return dado * POTENCIADOR_DE_AGUA;
}

void Usuario::imprimirEstadoJugador() {
	cout << "Unidades de Riego: " << this->verUnidadesRiegoActuales() << "    ";

	cout << "Creditos Disponibles: " << this->verCreditosActuales() << "    ";

	cout << "Capacidad del tanque de Agua: " << this->verCapacidadTanqueActual()
			<< "    ";

	cout << "Capacidad del Almacen: " << this->verCapacidadAlmacen() << endl;

}

string prepararNombreImpresionTerreno(ui numJugador, ui numTerreno, ui turnoAct){
	ostringstream nombre;

	nombre << "Jugador" << numJugador << "_" << "Terreno" << numTerreno << "_" << "Turno" << turnoAct;

	cout << "nombre del terreno actual: ";
	cout << nombre.str() << endl;
	return nombre.str();
}

void Usuario::imprimirEstadoTerrenos(ui numJugador, ui cantTurnosAct, CondicionesIniciales* condIniciales) {
	Lista<Terreno*>* terrenos = this->obtenerTerrenos();
	for (ui i = 1; i < terrenos->contarElementos() + 1; i++){
		Terreno* terrenoAct = terrenos->obtener(i);
		std::string nombre = prepararNombreImpresionTerreno(numJugador, i, cantTurnosAct);
		mostrarEstadoTerreno(terrenoAct, nombre);
	}

}

ui Usuario::verCapacidadTanqueActual() {
	return this->capacidadTanqueActual;
}

ui Usuario::verUnidadesRiegoActuales() {
	return this->unidadesRiegoActuales;
}

ui Usuario::verCreditosActuales() {
	return this->creditosActuales;
}

ui Usuario::verCapacidadAlmacen() {
	return this->almacen->verCapacidad();
}

ui Usuario::contarTerrenos(){
	Lista<Terreno*>* terrenos = this->obtenerTerrenos();
	return terrenos->contarElementos();
}

ui Usuario::obtenerNumJugador(){
	return this->numJugador;
}

Almacen* Usuario::obtenerAlmacen() {
	return this->almacen;
}

Lista<Terreno*>* Usuario::obtenerTerrenos() {
	return this->terrenos;
}

void Usuario::cambiarCapacidadTanqueActual(ui capacidadNueva) {
	this->capacidadTanqueActual = capacidadNueva;
}

void Usuario::setUnidadesDeRiego(ui unidadesDeRiegoNueva){
	this->unidadesRiegoActuales = unidadesDeRiegoNueva;
}

void Usuario::aumentarUnidadesRiego(ui valorDeIncremento) {
	this->unidadesRiegoActuales += valorDeIncremento;
}

void Usuario::reducirUnidadesRiego(ui valorDeReduccion) {
	this->unidadesRiegoActuales -= valorDeReduccion;
}

void Usuario::aumentarCreditos(ui valorDeIncremento) {
	this->creditosActuales += valorDeIncremento;
}

void Usuario::reducirCreditos(ui valorDeReduccion) {
	this->creditosActuales -= valorDeReduccion;
}

void Usuario::cambiarCapacidadAlmacen(ui CapacidadNueva) {
	this->almacen->setCapacidad(CapacidadNueva);
}

//Liberar Memoria Usuario.

void Usuario::liberarTerrenos() {
	Lista<Terreno*>* terrenos = this->obtenerTerrenos();
	ui cantTerrenos = terrenos->contarElementos();
	std::cout << "Antes de liberar terrenos: somos: " << cantTerrenos
			<< std::endl;
	for (ui i = cantTerrenos; i > 0; i--) {
		Terreno* terreno = terrenos->remover(i);
		delete terreno;
	}
}

Usuario::~Usuario() {
	this->liberarTerrenos();
	delete terrenos;
	delete almacen;
}




