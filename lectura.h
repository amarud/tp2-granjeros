#ifndef LECTURA_H_
#define LECTURA_H_

#include "Envio.h"
#include "Cultivo.h"
#include "Lista.h"
#include <string>

/*
 * Pre: cultivos fue inicializada.
 * Post: Almacenó en cultivos, Cultivos asociados dados por rutaArchivo.
 */
void cargarCultivos(std::string rutaArchivo, Lista<Cultivo*>* cultivos);

/*
 * Pre: envios fue inicializada.
 * Post: Almacenó en envios, Envios asociados dados por rutaArchivo.
 */
void cargarEnvios(std::string rutaArchivo, Lista<Envio*>* envios);


#endif /* LECTURA_H_ */
