/*
 * Parcela.h
 *
 *  Created on: 20 may. 2018
 *      Author: patron
 */

#ifndef PARCELA_H_
#define PARCELA_H_

#include "Cultivo.h"
#include <iostream> // NULL pointer.

/* Se encarga de procesar cada uno de los estados
 * posibles de la parcela durante el juego.
 * Alli además, se administraran tanto los tiempo de crecimiento
 * como de recuperacion de cada cultivo asociado.
 */
class Parcela {

private:

	bool ocupada;

	bool regada;

	bool recuperandose;

	Cultivo* tipoDeCultivo;

	ui tiempoDeCosecha;

	ui tiempoDeRecuperacion;

public:
	/*
	 * Post: Inicializó todos los valores de la clase parcela.
	 *
	 * Inicialmente la parcela está:
	 * .Libre
	 * .No Regada.
	 * .Recuperada.
	 * .Sin cultivo asociado(NULL).
	 *
	*/
	Parcela();

	/*
	 * Post: Devuelve un valor de verdad sobre si
	 * la parcela está actualmente ocupada(true) ó no(false).
	 */
	bool estaOcupada();

	/*
	 * Post: Devuelve un valor de verdad sobre si
	 * la parcela está actualmente regada(true) ó no(false).
	 */
	bool estaRegada();

	/*
	 * Post: devuelve el tiempo de cosecha que lleva actualmente la parcela ocupada.
	 * En caso de que no se encuentre ocupada, Devuelve 0;
	 */
	ui verTiempoDeCosecha();


	/*
	 * Post: devuelve el tiempo de recuperacion que debe esperar la parcela para ser nuevamente sembrada
	 * En caso de que se encuentre libre, Devuelve 0;
	 */
	ui verTiempoDeRecuperacion();

	/*
	 * Post: Carga a la parcela con un cultivo dado.
	 */
	void setCultivo(Cultivo* cultivo);

	/*
	 * Post : Modifica el tiempo de cosecha actual por tiempoDeCosecha.
	 */
	void setTiempoDeCosecha(ui tiempoDeCosecha);

	/*
	 * Post: Modifica el tiempo de recuperación actual por tiempoDeRecuperacion
	 */
	void setTiempoDeRecuperacion(ui tiempoDeRecuperacion);
	
	/*
	 * Post: Devuelve el cultivo asociado a la parcela.
	 * 		Null, en caso que la parcela esté libre.
	 */
	Cultivo* getCultivo();
	
	/*
	 * Post: Devuelve True si la parcela esta en recuperacion,
	 * 		False en caso contrario.
	 *
	 */
	bool enRecuperacion();

	/*
	 * Post: Cambia el estado de la parcela a Regada.
	 */
	void setParcelaRegada();

	/*
	 * Post: Cambia el estado de la parcela a Ocupada.
	 */
	void setParcelaOcupada();

	/*
	 * Post: Cambia el estado de la parcela a Libre.
	 */
	void setParcelaLibre();

	/*
	 * Post: Cambia el estado de riego a sin regar.
	 */
	void setParcelaSinRegar();

	/*
	 * Post: establece que la parcela entra en Recuperacion.
	 * Alli no se puede realizar ninguna accion hasta que vuelva a estar recuperada/libre.
	 */
	void setParcelaEnRecuperacion();

	/*
	 * Post: establece que la parcela esta recuperada y
	 * lista su utilizacion.
	 */
	void setParcelaRecuperada();

	/* Utilizada para el correcto seguimiento de
	 * los tiempos de cosecha y recuperacion de cada parcela ocupada.
	 */
	void imprimirEstado();

};

#endif /* PARCELA_H_ */
