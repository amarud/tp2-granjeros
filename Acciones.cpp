#include "Acciones.h"

using namespace std;

typedef unsigned int ui;

acciones_t Acciones::verAccion(){
	return this->accionAct;
}

void Acciones::cambioDeAccion(ui numPedido){
			
			switch (numPedido){
			case 1:
				this->accionAct = SEMBRAR; break;
			case 2:
				this->accionAct = REGAR; break;
			case 3:
				this->accionAct = COSECHAR; break;
			case 4:
				this->accionAct = ENVIAR; break;
			case 5:
				this->accionAct = COMPRAR_TERRENO; break;
			case 6:
				this->accionAct = VENDER_TERRENO; break;
			case 7:
				this->accionAct = AUMENTAR_CAP_TANQUE_DE_AGUA; break;
			case 8:
				this->accionAct = AUMENTAR_CAP_ALMACEN; break;
			case 9:
				this->accionAct = FINALIZAR_TURNO; break;
			}
		}


void Acciones::ejecutarAccion(Usuario* jugador, CondicionesIniciales* condIniciales){
	bool accionRealizada = false;
	switch (this->accionAct){
		case SEMBRAR:
			cout << "Elegiste: Sembrar" << endl;
			accionRealizada = this->sembrar(jugador, condIniciales);
			break;
		case REGAR:
			cout << "Elegiste: Regar" << endl;
			accionRealizada = this->regar(jugador);
			break;
		case COSECHAR:
			cout << "Elegiste: Cosechar" << endl;
			accionRealizada = this->cosechar(jugador,condIniciales);
			break;
		case ENVIAR:
			cout << "Elegiste: EnviarCultivoADestino" << endl;
			accionRealizada = this->enviar(jugador, condIniciales);
			break;
		case COMPRAR_TERRENO:
			cout << "Elegiste: ComprarTerreno" << endl;
			accionRealizada = this->comprarTerreno(jugador, condIniciales);
			break;
		case VENDER_TERRENO:
			cout << "Elegiste: VenderTerreno" << endl;
			accionRealizada = this->venderTerreno(jugador, condIniciales);
			break;
		case AUMENTAR_CAP_TANQUE_DE_AGUA:
			cout << "Elegiste: AumentarCapTanqueAgua" << endl;
			accionRealizada = this->aumentarCapTanqueAgua(jugador, condIniciales);
			break;
		case AUMENTAR_CAP_ALMACEN:
			cout << "Elegiste: AumentarCapAlmacen" << endl;
			accionRealizada = this->aumentarCapAlmacen(jugador, condIniciales);
			break;
		case FINALIZAR_TURNO:
			cout << "Elegiste: finalizarTurno" << endl;
			accionRealizada = this->finalizarTurno(jugador, condIniciales);
			break;
		}
	if(accionRealizada){
			cout<<"la operacion se realizo"<<endl;
		}
		else{
			cout<<" la operacion no se pudo realizar"<<endl;
		}
}
bool Acciones::sembrar(Usuario* jugador, CondicionesIniciales* condIniciales){
	bool sembrado = false;
	Terreno* terreno = obtenerTerreno(jugador->obtenerTerrenos());
	unsigned int fila = validarFila(terreno);
	unsigned int columna = validarColumna(terreno);
	Parcela** terrenoAsembrar = terreno->verTerreno();
	ui posCultivo = 1;
	if(!terrenoAsembrar[fila][columna].estaOcupada()){
		cout << "que cultivo desea sembrar?:" << endl;
		string nombreDeCultivo = validarNumCultivo();
		posCultivo = buscarPosCultivo(condIniciales->verCultivos(), nombreDeCultivo);
		Cultivo* cultivoActual = condIniciales->verCultivos()->obtener(posCultivo);
		terrenoAsembrar[fila][columna].setCultivo(cultivoActual);
		jugador->reducirCreditos(cultivoActual->verCostoSemilla());
		terrenoAsembrar[fila][columna].setParcelaOcupada();
		terrenoAsembrar[fila][columna].setParcelaRegada(); //Se asume que cuanto la sembrar ya está siendo regada.
		terrenoAsembrar[fila][columna].setTiempoDeCosecha(cultivoActual->verTiempoDeCosecha());
		terrenoAsembrar[fila][columna].setTiempoDeRecuperacion(cultivoActual->verTiempoDeRecup());

		sembrado=true;
	}
	else{
			cout << "La parcela elegida se encuentra ocupada/en recuperacion, elige otra" << endl;
		}
	return sembrado;
}

bool Acciones::regar(Usuario* jugador){
	Terreno* terreno = obtenerTerreno(jugador->obtenerTerrenos());
	ui fila = validarFila(terreno);
	ui columna = validarColumna(terreno);
	bool regado = false;
	Parcela** terrenoAregar = terreno->verTerreno();
	/*No se puede regar una parcela que no este ocupada, regada, recuperándose*/
	if ((!terrenoAregar[fila][columna].estaRegada()) && (terrenoAregar[fila][columna].estaOcupada())&&
			!terrenoAregar[fila][columna].enRecuperacion()){
		terrenoAregar[fila][columna].setParcelaRegada();
		Cultivo* cultivoARegar = terrenoAregar[fila][columna].getCultivo();
		ui ConsumoUnidadesRiegoCultivo = cultivoARegar->verConsumoRiego();
		jugador->reducirUnidadesRiego(ConsumoUnidadesRiegoCultivo); // el consumo de unidades de riego depende del cultivo que se quiera regar.
		regado = true;
	}
	else if (!terrenoAregar[fila][columna].estaOcupada()) {
		cout <<"Una parcela vacia no puede ser regada." << endl;
	}
	else if(terrenoAregar[fila][columna].enRecuperacion()){
		cout <<"Una parcela en recuperacion no puede ser regada"<<endl;
	}
	else {
		cout << "Esta parcela ya esta regada." << endl;
	}
	return regado;
}

bool existeCosechaParaEnviar(ui posCosecha){
	return posCosecha >= 0;
}


bool destinoEsValido(string a, string b){
	return a.compare(b) == 0;
}

bool cultivoEsValido(string nomCultivo, string nomCultivoEnvio){
	return nomCultivo.compare(nomCultivoEnvio) == 0;

}

bool envioEsPosible(string destinoUsuario, string destinoEnvio, string cultivoUsuario, string cultivoEnvio){
	return destinoEsValido(destinoEnvio, destinoUsuario) && cultivoEsValido(cultivoUsuario, cultivoEnvio);
}


bool Acciones::enviar(Usuario* jugador, CondicionesIniciales* condIniciales){
	bool enviado=false;
	string nomDestino;
	Lista<Cultivo*>* cosechas = jugador->obtenerAlmacen()->verCosechas();
	Lista<Envio*>* enviosDiponibles= condIniciales->verDestinos();
	cout<<"que cultivo desea enviar?" << endl;
	string nomCultivo = validarNumCultivo();
	ui posCosechaEnviar = buscarPosCultivo(cosechas, nomCultivo);
	cout << "a donde lo quieres enviar?:";
	cin >> nomDestino;
	if(existeCosechaParaEnviar(posCosechaEnviar)){
		enviosDiponibles->iniciarCursor();
		while(enviosDiponibles->avanzarCursor()){
			Envio* destino = enviosDiponibles->obtenerCursor();
			string posibleDestino = destino->verNombreDestino();
			string posibleCultivo = destino->verNomCultivo();
			if(envioEsPosible(nomDestino, posibleDestino, nomCultivo, posibleCultivo)){
				Cultivo* cultivoAEnviar = cosechas->remover(posCosechaEnviar);
				cout << "ganancia por envio: " << cultivoAEnviar->verRentabilidad() << "costo de envio: " << destino->verPrecio()<< endl;
				jugador->aumentarCreditos(cultivoAEnviar->verRentabilidad() - destino->verPrecio());
				cout<<"ganancia neta: "<< cultivoAEnviar->verRentabilidad() - destino->verPrecio() << endl;
				enviado = true;
			}
		}
		if(!enviado){
			cout << "el destino/nombre de cultivo no se encuentra dentro de los envios disponibles" << endl;
		}
	}
	else{
		cout << "la cosecha seleccionada no se encuentra en Almacen, para su envio." << endl;
	}
	return enviado;
}
bool Acciones::cosechar(Usuario* jugador, CondicionesIniciales* condIniciales){
	bool cosechado = false;
	Almacen* almacen = jugador->obtenerAlmacen();
	Lista<Terreno*>* listaDeTerrenos = jugador->obtenerTerrenos();
	Terreno* terreno = this->obtenerTerreno(listaDeTerrenos);
	Parcela** terrenoAcosechar = terreno->verTerreno();
	Cultivo* cultivo;
	ui fila = validarFila(terreno);
	ui columna = validarColumna(terreno);

	if(terrenoAcosechar[fila][columna].estaOcupada()&&
			(terrenoAcosechar[fila][columna].verTiempoDeCosecha()==0)&&
			!terrenoAcosechar[fila][columna].enRecuperacion()){

		cultivo=terrenoAcosechar[fila][columna].getCultivo();
		almacen->guardarCultivo(cultivo);
		terrenoAcosechar[fila][columna].setParcelaEnRecuperacion();
		cosechado = true;
	}
	else{
		cout<<"no se puede cosechar, parcela libre/en recuperación "<<endl;
	}
	return cosechado;
}

std::string Acciones::darCultivo(ui numDeCultivo){
	std::string nomCultivo;
	switch (numDeCultivo){
	case 1:
		nomCultivo = FRUTILLA;
		break;
	case 2:
		nomCultivo = PAPA;
		break;
	case 3:
		nomCultivo = LECHUGA;
		break;
	case 4:
		nomCultivo = KIWI;
		break;
	}
	return nomCultivo;
}

bool Acciones::numCultivoValido(ui numCultivo){
	return numCultivo > 0 && numCultivo <= MAX_CULTIVO_DISPONIBLE;
}

std::string Acciones::validarNumCultivo(){
	ui numDeCultivo;
	mostrarCultivosDisponibles();
	cin >> numDeCultivo;
	while(!numCultivoValido(numDeCultivo)){
		cout << "Número de cultivo invalido, Elija un número [1.." << MAX_CULTIVO_DISPONIBLE << "]" << endl;
		mostrarCultivosDisponibles();
		cout << "ingrese el cultivo:";
		cin >> numDeCultivo;
	}
	return darCultivo(numDeCultivo);

}

void Acciones::mostrarCultivosDisponibles(){
	cout << "1. Frutilla" << endl;
	cout << "2. Papa" << endl;
	cout << "3. Lechuga" << endl;
	cout << "4. Kiwi" << endl << endl;
}


bool Acciones::cultivoValido(Cultivo* cultivo, string nomCultivo){
	return cultivo->verNombre().compare(nomCultivo) == 0;
}

ui Acciones::buscarPosCultivo(Lista<Cultivo*>* cultivos, string nomCultivo){
	ui posCultivoAct = 1;
	Cultivo* cultivoAct;
	while(posCultivoAct < cultivos->contarElementos() + 1){
		cultivoAct = cultivos->obtener(posCultivoAct);
		if(cultivoValido(cultivoAct, nomCultivo)){
			return posCultivoAct;
		}
		posCultivoAct++;
	}
	return -1;
}


Terreno* Acciones::obtenerTerreno(Lista<Terreno*>* listaDeTerrenos){
	ui posicion = validadorPosicionTerreno(listaDeTerrenos);
	Terreno* terreno = listaDeTerrenos->obtener(posicion);
	return terreno;
}

bool posTerrenoValida(ui posAct, ui posMin, ui posMax){
	return posAct >= posMin && posAct <= posMax;
}

ui Acciones::validadorPosicionTerreno(Lista<Terreno*>* listaDeTerrenos){
	ui posicionTerreno;
	ui maxCantidadTerrenos = listaDeTerrenos->contarElementos();
	cout << "ingrese el numero de terreno: ";
	cin >> posicionTerreno;
	while(!posTerrenoValida(posicionTerreno, 1, maxCantidadTerrenos)){
		cout << "Ha ingresado un numero incorrecto, vuelva a ingresar." << endl;
		cout << "ingrese el numero de terreno: ";
		cin >> posicionTerreno;
	}
	
	return posicionTerreno;
}

bool Acciones::almacenarCosecha(Almacen* almacen, Cultivo* nombreDeCultivo){
	bool almacenado = true;
	if(almacen->verCapacidad() > almacen->verCantidadDeCultivos()){
		almacen->guardarCultivo(nombreDeCultivo);
	}
	else{
		cout << "no hay capacidad en el almacen" << endl;
		almacenado = false;
	}
	return almacenado;
}

ui Acciones::validarFila(Terreno* terreno){
	ui fila, maxFila;
	maxFila = terreno->verAlto();
	
	cout << "ingrese la fila: ";
	cin >> fila;
	while(fila < 1 || fila > maxFila){
		cout<<"ha ingresado un valor fuera de rango" << endl;
		cout<<"ingrese nuevamente la fila: ";
		cin >> fila;
	}
	fila--;
	return fila;
}

ui Acciones::validarColumna(Terreno* terreno){
	ui columna, maxColumna;
	maxColumna = terreno->verAncho();
	
	cout << "ingrese la columna: ";
	cin >> columna;
	while(columna < 1 || columna > maxColumna){
		cout<<"ha ingresado un valor fuera de rango" << endl;
		cout<< "ingrese nuevamente la columna: ";
		cin >> columna;
	}
	columna--;
	return columna;
}


bool esPosibleCompra(ui creditosJugadorAct, ui valorTerreno ){
	return creditosJugadorAct >= valorTerreno;
}

bool Acciones::comprarTerreno(Usuario* jugador,CondicionesIniciales* condIniciales){
	ui creditosJugadorAct = jugador->verCreditosActuales();
	ui valorCompraTerreno = condIniciales->calcularValorCompraTerreno(jugador->contarTerrenos());
	if(esPosibleCompra(creditosJugadorAct, valorCompraTerreno)){
		jugador->agregarTerreno(condIniciales->obtenerAltoTerreno(), condIniciales->obtenerAnchoTerreno());
		jugador->reducirCreditos(valorCompraTerreno);
		return true;
	}
	cout << "No se pudo realizar la compra del terreno. No posee creditos suficientes." << endl;
	return false;
}


bool Acciones::aumentarCapTanqueAgua(Usuario* jugador, CondicionesIniciales* condIniciales){
	bool accionCompletada = false;
	ui valorAumentarCapTanqueAgua = condIniciales->calcularValorAumentarCapacidadTanque();
	if (jugador->verCreditosActuales() >= valorAumentarCapTanqueAgua) {
		jugador->reducirCreditos(valorAumentarCapTanqueAgua);
		jugador->cambiarCapacidadTanqueActual(jugador->verCapacidadTanqueActual() * COF_AMPLIACION_CAPACIDAD_TANQUE);
		accionCompletada = true;
		cout << "La capacidad del tanque de agua fue aumentada" << endl;
	} else {
		cout << "No tienes los creditos suficientes para aumentar la capacidad del tanque de agua" << endl;
	}
	return accionCompletada;
}

bool Acciones::aumentarCapAlmacen(Usuario* jugador, CondicionesIniciales* condIniciales){
	bool accionCompletada = false;
	ui valorAumentarCapAlmacen = condIniciales->calcularValorAumentarCapacidadAlmacen();
	if (jugador->verCreditosActuales() >= valorAumentarCapAlmacen) {
		jugador->reducirCreditos(valorAumentarCapAlmacen);
		jugador->cambiarCapacidadAlmacen(jugador->verCapacidadAlmacen() * COF_AMPLIACION_CAPACIDAD_ALMACEN);
		accionCompletada = true;
		cout << "La capacidad del almacen fue aumentada" << endl;
	} else {
		cout << "No tienes los creditos suficientes para aumentar la capacidad del almacen" << endl;
	}
	return accionCompletada;
}

bool Acciones::venderTerreno(Usuario* jugador, CondicionesIniciales* condIniciales){
	ui posicionTerreno= this->validadorPosicionTerreno(jugador->obtenerTerrenos());
	ui precio = condIniciales->calcularValorVentaTerreno(jugador->obtenerTerrenos()->contarElementos() - 1);
	cout<<"el terreno seleccionado se vendio a un precio de: "<< precio <<endl;
	jugador->obtenerTerrenos()->remover(posicionTerreno);
	return true;
}


/******** FINALIZAR ********/


void Acciones::actualizarEstadoTerrenos(Usuario* jugador){
	unsigned int tamanio= jugador->obtenerTerrenos()->contarElementos();
	Terreno* terreno;

	for(ui i=1; i<= tamanio; i++){
		terreno=jugador->obtenerTerrenos()->obtener(i);
		actualizarParcelas(terreno);
	}
}


void Acciones::actualizarParcelas(Terreno* terrenoAct){
	ui altoTerreno = terrenoAct->verAlto();
	ui anchoTerreno = terrenoAct->verAncho();
	Parcela** terreno = terrenoAct->verTerreno();
	for(ui j = 0; j < altoTerreno; j++){
		for(ui k = 0; k < anchoTerreno; k++){
			if(terreno[j][k].estaOcupada()){
				cout << "******* Parcela Antes de finalizar Turno: ********* " << endl;
				terreno[j][k].imprimirEstado();
				//.Parcela se pudre al no haber sido cosechada a tiempo.
				if((terreno[j][k].verTiempoDeCosecha()==0 )&& // Si esta regada o no. no es de interes al momento de podrirse xq la fruta ya está madura XD.
						(!terreno[j][k].enRecuperacion())){
					terreno[j][k].setTiempoDeRecuperacion(terreno[j][k].getCultivo()->verTiempoDeRecup()/2 );
					terreno[j][k].setParcelaEnRecuperacion();
				}
				//.Parcela no fue regada, entonces se seco. tiempo de recuperacion normal.
				else if(!terreno[j][k].estaRegada() && !terreno[j][k].enRecuperacion()){
					terreno[j][k].setTiempoDeCosecha(0); //Una forma de reinicializar el conteo de la cosecha, sin modificar la parcela completa (Sin crearla nuevamente)
					terreno[j][k].setParcelaEnRecuperacion();
				}
				ajustarTiempoTerreno(&terreno[j][k]);
				cout << "******** Parcela luego de finalizar Turno: ********" << endl;
				terreno[j][k].imprimirEstado();
			}
		}
	}
}
void Acciones::ajustarTiempoTerreno(Parcela* terreno){
	if(terreno->enRecuperacion() && terreno->verTiempoDeRecuperacion() > 0){

			terreno->setTiempoDeRecuperacion(  terreno->verTiempoDeRecuperacion()-1);

		}

	else if(terreno->enRecuperacion() && terreno->verTiempoDeRecuperacion()==0){
		terreno->setParcelaLibre();
		terreno->setParcelaSinRegar();
		terreno->setParcelaRecuperada();
	}
	else {
		terreno->setTiempoDeCosecha(  terreno->verTiempoDeCosecha() - 1);
		terreno->setParcelaSinRegar();
	}


}

void Acciones::actualizarCapacidadTanque(Usuario* jugador){
	ui capacidadTanqueJugador = jugador->verCapacidadTanqueActual();
	if(jugador->verUnidadesRiegoActuales() > capacidadTanqueJugador){
		jugador->setUnidadesDeRiego(capacidadTanqueJugador);
	}
}

void Acciones::validar_stock_almacen(Usuario* jugador){
	Almacen* almacenJugador = jugador->obtenerAlmacen();
	if (!almacenJugador->hayCapacidad()){
		Lista<Cultivo*>* cosechas = almacenJugador->verCosechas();
		ui ultimaPosicionCosecha = cosechas->contarElementos();
		while(ultimaPosicionCosecha > almacenJugador->verCapacidad()){
			cosechas->remover(ultimaPosicionCosecha);
			ultimaPosicionCosecha--;
		}
	}
}

bool Acciones::finalizarTurno(Usuario* jugador, CondicionesIniciales* condIniciales){
	actualizarEstadoTerrenos(jugador);
	actualizarCapacidadTanque(jugador);
	validar_stock_almacen(jugador);
	return true;

}

