/*
 * principal.cpp
 *
 *  Created on: 21/5/2018
 *      Author: brian
 */
#include "Granjeros.h"
#include <iostream>

int main(int argc, char** argv){

	Granjeros juego;
	juego.prepararJuego();
	juego.jugarGranjeros();
	juego.mostrarResultadosPartida();

	return 0;
}
