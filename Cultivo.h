/*
 * cultivo.h
 *
 *  Created on: 06/05/2018
 *      Author: patron
 */

#ifndef CULTIVO_H_
#define CULTIVO_H_

typedef unsigned int ui;
#include <string>

/* Encargada de almacenar las caracteristicas de cada cultivo,
 * de manera independiente cada uno de ellos.
 */
class Cultivo{
	private:
		std::string nombre;
		ui costoSemilla;
		ui tiempoCosecha;
		ui rentabilidad; //Ganancia al cosechar,luego de enviar a destino.
		ui tiempoDeRecup;
		ui consumoRiego; //Nuevo respecto del TP1

	public:

		/* Constructor con parámetros.
		 * Pre: Los parámetros nombre, costoSemilla, tiempoCosecha,Rentabilidad, tiempoRecup, consumoRiego
		 * fueron correctamente leídos por el archivo de cultivos correspondiente.
		 *
		 * Post: Crea una instancia de cultivo con sus atributos inicializados.
		 */
		Cultivo(std::string nombre, ui costoSemilla, ui tiempoCosecha, ui rentabilidad, ui tiempoRecup, ui consumoRiego);

		/*
		 * Post: Devuelve la ganancia de creditos que se obtiene por el cultivo,
		 *  en caso de lograr cosechar y enviar.
		 */
		ui verRentabilidad();

		/*
		 * Post: Devuelve el costo de la semilla para su siembra.
		 */
		ui verCostoSemilla();

		/*
		 * Post: Devuelve el nombre del cultivo.
		 */
		std::string  verNombre();

		/*
		 * Post: Devuelve el tiempo de recuperacion del cultivo, una vez cosechado
		 *  o bien luego de haberse secado.
		 */
		ui verTiempoDeRecup();

		/*
		 * Post: Devuelve el tiempo que tarda el cutlvio en crecer hasta ser cosechado.
		 */
		ui verTiempoDeCosecha();

		/*
		 * Post: Devuelve la cantidad de unidades de riego que necesita el cultivo
		 *  para ser regado cada vez que se requiera
		 */
		ui verConsumoRiego();

		/* Destructor de cultivo
		 * Post: libera la memoria solicitada al instanciarse.
		 */
		~Cultivo();

};







#endif /* CULTIVO_H_ */
