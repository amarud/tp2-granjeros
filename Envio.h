/*
 * envio.h
 *
 *  Created on: 12/05/2018
 *      Author: patron
 */

#ifndef ENVIO_H_
#define ENVIO_H_

#include <string>

typedef unsigned int ui;


class Envio{
	private:
		std::string nombreDestino;
		std::string nomCultivo;
		ui distancia; //KM
		ui precio; // para ser consistente con la unidad del juego, Monedas(enteras).

	public:

		/* Constructor de la clase envio
		 * Pre: Los datos ingresados, están correctamente validados sobre el archivo.
		 * Post: Inicializó un envío posible durante el juego.
		 * */
		Envio(std::string destino, ui distancia, ui precio, std::string cultivo);

		/*
		* Pre: Envio Fue creado.
		* Post: Devuelve el nombre del cultivo asociado al posible envio.
		*/
		std::string verNomCultivo();

		/*
		 * Pre: Envio fue creado.
		 * Post: Devuelve el nombre del posible destino.
		 */
		std::string verNombreDestino();

		/*
		* Pre: Envio fue creado.
		* Post: Devuelve la distancia que separa el origen del destino.
		*/
		ui verDistancia();

		/*
		* Pre: Envio fue creado.
		* Post: Devuelve el costo de envio desde origen a destino.
		*/
		ui verPrecio();

		/* Destructor de envio
		 */
		~Envio();

};



#endif /* ENVIO_H_ */
