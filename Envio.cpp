/*
 * Envio.cpp
 *
 *  Created on: 14/05/2018
 *      Author: patron
 */

#include "Envio.h"

Envio::Envio(std::string destino, ui distancia, ui precio, std::string cultivo){

	this->nombreDestino = destino;
	this->distancia = distancia;
	this->precio = precio;
	this->nomCultivo = cultivo;

}

std::string Envio::verNombreDestino(){
	return this->nombreDestino;
}
ui Envio::verDistancia(){
	return this->distancia;
}
ui Envio::verPrecio(){
	return this->precio;
}
std::string Envio::verNomCultivo(){
	return this->nomCultivo;
}

Envio::~Envio(){
}


