/*
 * Cultivo.cpp
 *
 *  Created on: 13/05/2018
 *      Author: patron
 */


#include "Cultivo.h"

Cultivo::Cultivo(std::string nombre, ui costoSemilla, ui tiempoCosecha, ui rentabilidad, ui tiempoRecup, ui consumoRiego){
	this->nombre = nombre;
	this->costoSemilla = costoSemilla;
	this->tiempoCosecha = tiempoCosecha;
	this->rentabilidad = rentabilidad;
	this->tiempoDeRecup = tiempoRecup;
	this->consumoRiego = consumoRiego;
}

ui Cultivo::verRentabilidad(){
	return this->rentabilidad;
}

ui Cultivo::verCostoSemilla(){
	return this->costoSemilla;
}

std::string Cultivo::verNombre(){
	return this->nombre;
}

ui Cultivo::verTiempoDeRecup(){
	return this->tiempoDeRecup;
}

ui Cultivo::verTiempoDeCosecha(){
	return this->tiempoCosecha;
}

ui Cultivo::verConsumoRiego(){
	return this->consumoRiego;
}

Cultivo::~Cultivo(){
}
