/*
 * CondicionesIniciales.cpp
 *
 *  Created on: 13/05/2018
 *      Author: patron
 */

#include "ConstantesyTipos.h"
#include "CondicionesIniciales.h"

#include "lectura.h"


/******** Privadas ********/
ui CondicionesIniciales::calcularCreditos(ui alto,ui ancho,float dificultadReduccion){
	return COF_CREDITOS_INICIALES * alto * ancho * dificultadReduccion;
}

ui CondicionesIniciales::calcularCapTanque(ui alto,ui ancho,float dificultadReduccion){
	return alto * ancho * dificultadReduccion;
}

ui CondicionesIniciales::calcularCapAlmacen(ui alto,ui ancho,float dificultadReduccion){
	return (COF_CAPACIDAD_ALMACEN *(alto + ancho)) * dificultadReduccion;
}

/********  Publicas  ********/

CondicionesIniciales::CondicionesIniciales(){
	this->cultivos = new Lista<Cultivo*>(); // crea lista de cultivos vacia.
	this->destinos = new Lista<Envio*>();
	this->capacidadTanque = 0;
	this->capacidadAlmacen = 0;
	this->creditos = 0;
	this->alto = 0;// datos dados por el usuario.
	this->ancho = 0;//
	this->dificultadReduccion = 0;
	this->dificultadAmpliacion = 0;
	cargarArchivos();

}

float asignarDificultadReduccion(ui dificultad){
	if (dificultad == 1)
		return COF_REDUCCION_NIVEL_FACIL;
	else if (dificultad == 2)
		return COF_REDUCCION_NIVEL_MEDIO;
	return COF_REDUCCION_NIVEL_DIFICIL;
}

float asignarDificultadAmpliacion(ui dificultad){
	if (dificultad == 1)
		return COF_AMPLIACION_NIVEL_FACIL;
	else if (dificultad ==2)
		return COF_AMPLIACION_NIVEL_MEDIO;
	return COF_AMPLIACION_NIVEL_DIFICIL;
}

void CondicionesIniciales::prepararParametrosIniciales(ui dificultad, ui ancho, ui alto){
	//Unidades de riego iniciales (ELminiar CTE).
	this->dificultadReduccion = asignarDificultadReduccion(dificultad);
	this->dificultadAmpliacion = asignarDificultadAmpliacion(dificultad);
	this->capacidadTanque = calcularCapTanque(alto, ancho, dificultadReduccion); // se define según el nivel elegido por el usuario (Usar un coeficiente).
	this->capacidadAlmacen = calcularCapAlmacen(alto,ancho,dificultadReduccion);
	this->creditos = calcularCreditos(alto, ancho, dificultadReduccion); // idem punto de arriba.
	this->alto = alto;// datos dados por el usuario.
	this->ancho = ancho;//

}

ui CondicionesIniciales::calcularValorCompraTerreno(ui cantTerrenos){
	float dificultad = this->obtenerDificultadEnAmpliacion();
	ui valorCompraTerreno = (PRECIO_INICIAL_TERRENO + (cantTerrenos - 1)) * dificultad;
	std::cout << "valor de compra terreno con: "<< cantTerrenos<<"terrenos, es:"<< valorCompraTerreno << std::endl;
	return valorCompraTerreno;
}

ui CondicionesIniciales::calcularValorVentaTerreno(ui cantTerrenos){

	ui valorCompraTerreno = calcularValorCompraTerreno(cantTerrenos)/COF_VENTA_TERRENO;

	return valorCompraTerreno;
}

ui CondicionesIniciales::calcularValorAumentarCapacidadAlmacen(){
	ui dificultad = this->obtenerDificultadEnAmpliacion();
	ui alto = this->alto;
	ui ancho = this->ancho;
	ui valorNuevaCapacidadAlmacen = alto * ancho * dificultad;
	std::cout<<"el valor para aumentar almacen en nivel"<<dificultad << "es:"<< valorNuevaCapacidadAlmacen<<std::endl;
	return valorNuevaCapacidadAlmacen;

}

ui CondicionesIniciales::calcularValorAumentarCapacidadTanque(){
	float dificultad = this->obtenerDificultadEnAmpliacion();
	ui alto = this->alto;
	ui ancho = this->ancho;
	ui valorNuevaCapacidadTanque = (alto + ancho)* ancho * dificultad;
	std::cout<<"el valor para aumentar tanque en nivel"<< dificultad << "es:"<< valorNuevaCapacidadTanque<<std::endl;
	return valorNuevaCapacidadTanque;
}

ui CondicionesIniciales::obtenerCreditos(){
	return this->creditos;
}

ui CondicionesIniciales::obtenerCapacidadTanque(){
	return this->capacidadTanque;
}

ui CondicionesIniciales::obtenerAltoTerreno(){
	return this->alto;
}

ui CondicionesIniciales::obtenerAnchoTerreno(){
	return this->ancho;
}

ui CondicionesIniciales::obtenerCapacidadAlmacen(){
	return this->capacidadAlmacen;
}

float CondicionesIniciales::obtenerDificultadEnAmpliacion(){
	return this->dificultadAmpliacion;
}


float CondicionesIniciales::obtenerDificultadEnReduccion(){
	return this->dificultadReduccion;
}

void CondicionesIniciales::liberarCultivos(){
	Lista<Cultivo*>* cultivos = this->verCultivos();
	ui cantCultivos = cultivos->contarElementos();
	std::cout<< "Antes de liberar cultivos: somos: " << cantCultivos << std::endl;
	for (ui i = cantCultivos ; i > 0 ; i--){
		Cultivo* cultivoAct = cultivos->remover(i);
		delete cultivoAct;
	}
}

void CondicionesIniciales::liberarEnvios(){
	Lista<Envio*>* envios = this->verDestinos();
	ui cantenvios = envios->contarElementos();
	std::cout<< "Antes de liberar envios: somos: " << cantenvios << std::endl;
	for (ui i = cantenvios ; i > 0 ; i--){
		Envio* envioAct = envios->remover(i);
		delete envioAct;
	}
}

CondicionesIniciales::~CondicionesIniciales(){
	liberarCultivos();
	liberarEnvios();
	delete this->destinos;
	delete this->cultivos;
}

Lista<Envio*>* CondicionesIniciales::verDestinos(){
	return this->destinos;
}

Lista<Cultivo*>* CondicionesIniciales::verCultivos(){
	return this->cultivos;
}

void CondicionesIniciales::cargarArchivos(){
	cargarCultivos(ARCHIVO_CULTIVOS, this->verCultivos());
	cargarEnvios(ARCHIVO_DESTINOS, this->verDestinos());
}
