/*
 * Terreno.h
 *
 *  Created on: 16/05/2018
 *      Author: patron
 */

#ifndef TERRENO_H_
#define TERRENO_H_

#include "CondicionesIniciales.h"
#include "Parcela.h"
#include "Lista.h"

//#define NULL 0


/*
 * Clase que se encarga de almacenar el terreno disponible/s
 * que puede tener cada Usuario.
 * Interviene escencialmente en la forma en que el terreno se almacenará en memoria.
 */
class Terreno {
private:
	Parcela** terreno;
	ui alto;
	ui ancho;

	/*Inicializa en memoria un terreno de parcela con las medidas dadas.*/
	Parcela** inicializarTerreno(const ui alto,const ui ancho);

public:
	/*Constructor con parámetros de Terreno.
	 */
	Terreno(ui alto, ui ancho);

	/*
	 * Pre: Terreno Fue creado.
	 * Post: Devuelve una implementación del terreno.
	 */
	Parcela** verTerreno();

	/*
	 * Pre: -
	 * Post: devuelve el alto del terreno Actual.
	 */
	ui verAlto();

	/* Pre: -
	 * Post: Devuelve el ancho del terreno Actual.
	 */
	ui verAncho();

	/* Destructor del Terreno.
	 * Post: Se liberó la memoria solicitada al crear el terreno.
	 */
	~Terreno();
	
};

#endif /* TERRENO_H_ */

