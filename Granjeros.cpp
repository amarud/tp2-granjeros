/*
 * Granjeros.cpp
 *
 *  Created on: 22/5/2018
 *      Author: brian
 */
#include "Granjeros.h"
#include "ConstantesyTipos.h"
#include <iostream>


using namespace std;

Granjeros::Granjeros(){
	jugadores = new Lista<Usuario*>();
	parametrosIniciales = new CondicionesIniciales();
	accionDisponible = new Acciones();
	turnos = 0;
	cantJugadores = 0;
	dificultad = 0;
	ancho = 0;
	alto = 0;
}

ui Granjeros::obtenerCantJugadores(){
	return this->cantJugadores;
}

ui Granjeros::obtenerCantidadTurnos(){
	return this->turnos;
}

Lista<Usuario*>* Granjeros::obtenerJugadores(){
	return this->jugadores;
}

Acciones* Granjeros::obtenerAccion(){
	return this->accionDisponible;
}

Granjeros::~Granjeros(){
	Lista<Usuario*>* jugadores = this->obtenerJugadores();
	ui cantJugadores = jugadores->contarElementos();
	cout << "Antes de liberar jugadores: somos: " << cantJugadores << endl;
	for (ui i = cantJugadores ; i > 0 ; i--){
		Usuario* jugador = jugadores->remover(i);
		delete jugador;
	}
	delete this->jugadores;
	delete this->parametrosIniciales;
	delete this->accionDisponible;
}

void Granjeros::prepararJuego(){

	cout<< BIENVENIDA_JUEGO << endl << endl;
	this->cantJugadores = leerCantJugadores();
	this->dificultad = leerDificultad();
	this->turnos= leerTurnos();
	this->ancho = pedirAncho();
	this->alto = pedirAlto();
	prepararJugadores();
}
ui Granjeros::leerTurnos(){
	ui turnosAux;
	cout << SOLICITUD_CANT_TURNOS;
	cin >> turnosAux;
	return turnosAux;
}

void Granjeros::prepararJugadores(){
	ui numJugador = 1;
	for (ui i = 0; i < this->obtenerCantJugadores(); i++ )
	{
		Usuario* jugador = new Usuario();
		cout<< "Preparando al jugador " << (i+1) << endl;
		prepararJugador(jugador, numJugador);
		jugadores->agregar(jugador);
		numJugador++;
	}
}

ui Granjeros::pedirAncho(){
	
	bool datoValido = false;
	
	ui ancho;
    
	cout << SOLICITUD_ANCHO_TERRENO << endl;
    
	do {
	
		cin >> ancho;
    
	    cout << endl;

		if ((ancho < 0) || (ancho >= MAX_ANCHO)) {
	
			datoValido = false;
	
			cout<<"EL ancho ingresado no es valido, por favor ingrese otra valor"<<endl;
		}
		else {
		    datoValido = true;
		}
	}
	while (!datoValido);
	
	return ancho;
}

ui Granjeros::pedirAlto(){
	
	bool datoValido = false;
	
	ui alto;
    
	cout << SOLICITUD_ALTO_TERRENO << endl;
    
	do {
	
		cin >> alto;
    
	    cout << endl;
	
		if ((alto < 0) || (alto >= MAX_ALTO)) {
	
			datoValido = false;
	
			cout<<"EL alto ingresado no es valido, por favor ingrese otra valor"<<endl;
		}
		else {
		    datoValido = true;
		}
	}
	while (!datoValido);
	
	return alto;
}

void Granjeros::prepararJugador( Usuario* jugador, ui numJugador)
{
	CondicionesIniciales* CIJugador = this->parametrosIniciales;
	CIJugador->prepararParametrosIniciales(dificultad, alto, ancho);
	jugador->cargarParametrosIniciales(CIJugador, numJugador);
};


/* Metodos privados auxiliares para lectura y validacion */

ui Granjeros::leerCantJugadores(){ // Uniformizando la escritura de metodos llave arriba
	do{
		cout << SOLICITUD_CANT_JUGADORES;
		cin >> cantJugadores;
		if ( !cantJugadoresValida (cantJugadores) ){ // llaves de apertura y cierre de if - else. ver consideraciones TP enunciado.
			cout<<"RANGO INVALIDO O SUPERIOR AL MAXIMO DE 4 JUGADORES!" << endl;
		}
	}while( !cantJugadoresValida (cantJugadores) );
	return this->cantJugadores;
}

bool Granjeros::cantJugadoresValida( int cantJugadores){
	return ( cantJugadores >= JUGADORES_MINIMOS ) && ( cantJugadores <= JUGADORES_MAXIMOS );
}

void Granjeros::mostrarDificultadJuego(){
	cout << "1. Facil" << endl;
	cout << "2. Medio" << endl;
	cout << "3. Dificil" << endl << endl;
}

bool Granjeros::dificultadValida( float dificultad)
{
	return (( dificultad >= DIFICULTAD_MINIMA ) && ( dificultad <= DIFICULTAD_MAXIMA ));
}

ui Granjeros::leerDificultad(){
	do{
		cout << SOLICITUD_DIFICULTAD_JUEGO << endl;
		mostrarDificultadJuego();
		cout<< "Difultad elegida: ";
		cin>>dificultad;
		if ( !dificultadValida(dificultad) ){
			cout<<"DIFICULTAD INVALIDA!" << endl;
		}
	}while( !dificultadValida(dificultad) );
	return this->dificultad;
}

void Granjeros::mostrarAccionesJuego(){
	cout << "1. Comprar y sembrar semilla" << endl;
	cout << "2. Regar Parcela" << endl;
	cout << "3. Cosechar Parcela" << endl;
	cout << "4. Enviar Cultivo a Destino" << endl;
	cout << "5. Comprar Terreno" << endl;
	cout << "6. Vender Terreno" << endl;
	cout << "7. Aumentar Capacidad Tanque" << endl;
	cout << "8. Aumentar Capacidad Almacen" << endl;
	cout << "9. Finalizar Turno" << endl << endl;
}

bool accionValida(ui numAccion){
	return numAccion > 0 && numAccion <= MAX_ACCION_DISPONIBLE;
}

ui Granjeros::solicitarAccion(){
	ui numAccion;
	this->mostrarAccionesJuego();
	cout << "Elija una accion a realizar:";
	do {
		cin >> numAccion;
		if ( !accionValida(numAccion)){
			cout<<"RANGO INVALIDO O SUPERIOR AL MAXIMO DE OPCIONES DISPONIBLES" << endl;
		}
	} while(!accionValida(numAccion));
	return numAccion;
}

void Granjeros::jugarGranjeros(){
	ui cantTurnosAct = 1;
	ui posJugador = 1;
	while (cantTurnosAct < this->obtenerCantidadTurnos() + 1) {
		Lista < Usuario*>* jugadores = this->obtenerJugadores();
		while( posJugador < jugadores->contarElementos() + 1 ) {
			Usuario * jugadorAct = jugadores->obtener(posJugador);
			ui dado = jugadorAct->tirarDado();
			jugadorAct->aumentarUnidadesRiego(jugadorAct->calcularUnidadesRiego(dado));
			cout << "Tus unidades de riego han aumentado! Ahora Tienes" << jugadorAct->verUnidadesRiegoActuales() << endl;
			jugadorAct->imprimirEstadoTerrenos(posJugador, cantTurnosAct, this->parametrosIniciales);
			jugadorAct->imprimirEstadoJugador();
			Acciones* accionAct = this->obtenerAccion();
			accionAct->cambioDeAccion( this->solicitarAccion() );
			while(accionAct->verAccion() != FINALIZAR_TURNO){
				jugadorAct->imprimirEstadoJugador();
				accionAct->ejecutarAccion(jugadorAct, this->parametrosIniciales);
				accionAct->cambioDeAccion( this->solicitarAccion() );
			}
			posJugador++; // el jugador finaliza el turno, por ende
			// los cambios en las acciones se veran afectados en el siguiente jugador.
			accionAct->ejecutarAccion(jugadorAct, this->parametrosIniciales); // Ejecuta Finalizar Turno.
		}
		posJugador = 1; // al comenzar un nuevo turno, el jugador 1 arranca nuevamente.
		cantTurnosAct++;
	}

}

void Granjeros::mostrarRankingPorJugador(){
	cout << "Fin del juego! Aquí la tabla de jugadores:" << endl;
	Lista<Usuario*>* jugadores = this->obtenerJugadores();
	jugadores->iniciarCursor();
	while(jugadores->avanzarCursor()){
		Usuario* jugadorAct = jugadores->obtenerCursor();
		cout << "Jugador: " << jugadorAct->obtenerNumJugador() << "Creditos: " << jugadorAct->verCreditosActuales() << endl;
	}
	jugadores->iniciarCursor();

}

/* Metodo de ordenamiento utilizado: Seleccion. */

ui Granjeros::buscarMax(Lista<Usuario*>* jugadores, ui ultimaPosicion){
	ui posMax = 1;
	for(ui posAct = posMax + 1; posAct < ultimaPosicion + 1; posAct++){
		Usuario* usuarioAnt = jugadores->obtener(posMax);
		Usuario* usuarioAct = jugadores->obtener(posAct);
		if (usuarioAct->verCreditosActuales() > usuarioAnt->verCreditosActuales()){
			posMax = posAct;
		}
	}
	return posMax;
}

void swap(Lista<Usuario*>* jugadores, ui posMax){
	Usuario* maxUsuario = jugadores->remover(posMax);
	jugadores->agregar(maxUsuario);
}

void Granjeros::ordenarJugadoresPorCreditos(){
	Lista<Usuario*>* jugadores = this->obtenerJugadores();
	for(ui ultimaPosicion = this->obtenerCantJugadores() ; ultimaPosicion > 0 ; ultimaPosicion--){
		ui posMax = this->buscarMax(jugadores, ultimaPosicion);
		swap(jugadores, posMax);
	}

}

void Granjeros::mostrarResultadosPartida(){
	this->ordenarJugadoresPorCreditos();
	this->mostrarRankingPorJugador();

}

