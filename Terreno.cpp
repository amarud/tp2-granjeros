/*
 * Terreno.cpp
 *
 *  Created on: 16/05/2018
 *      Author: patron
 */

#include "Terreno.h"


Parcela** Terreno::inicializarTerreno(const ui alto,const ui ancho)
{
	Parcela** tabla = new Parcela*[alto];
	for (ui i=0 ; i < alto; i++)
	{
		tabla[i] = new Parcela[ancho];
	}
	return tabla;
}



Terreno::Terreno(ui alto, ui ancho) {
	this->terreno = inicializarTerreno(alto, ancho);
	this->alto = alto;
	this->ancho = ancho;
}

ui Terreno::verAlto(){
	return this->alto;
}

ui Terreno::verAncho(){
	return this->ancho;
}

Terreno::~Terreno()
{
	for (ui i = 0; i < this->alto; i++)
	{
		delete[]this->terreno[i];
	} 
	delete[]this->terreno;
}

Parcela** Terreno::verTerreno(){
	return this->terreno;
}

