/*
 * ConstantesyTipos.h
 *
 *  Created on: 22/5/2018
 *      Author: brian
 */

#ifndef CONSTANTESYTIPOS_H_
#define CONSTANTESYTIPOS_H_

#include <string>

	typedef unsigned int ui;

	/* Constantes de validacion */

	const int JUGADORES_MAXIMOS = 4;
	const int JUGADORES_MINIMOS = 1;

	const ui MAX_CULTIVO_DISPONIBLE = 4;
	const ui MAX_ACCION_DISPONIBLE = 9;

	const int DIFICULTAD_MINIMA = 1;
	const int DIFICULTAD_MAXIMA = 3;
	
	const ui MAX_ALTO = 10;
	const ui MAX_ANCHO = 10;

	/* Constantes iniciales del juego */

		/*Utilizado para el calculo de las Condiciones iniciales del juego según la dificultad.*/
		const float COF_REDUCCION_NIVEL_FACIL = 1.00;
		const float COF_REDUCCION_NIVEL_MEDIO = 0.75;
		const float COF_REDUCCION_NIVEL_DIFICIL = 0.5;

		/*Utilizado en el calculo para la compra de un terreno -
		 * Incremento de capacidad de Tanque - Almacen.*/
		const float COF_AMPLIACION_NIVEL_FACIL = 1.00;
		const float COF_AMPLIACION_NIVEL_MEDIO = 1.5;
		const float COF_AMPLIACION_NIVEL_DIFICIL = 2.00;

		const ui UNIDADES_RIEGO_INICIALES = 0;
		const ui POTENCIADOR_DE_AGUA = 5;
		const ui COF_CREDITOS_INICIALES = 2;
		const ui COF_CAPACIDAD_ALMACEN = 2;
		const ui COF_INCREMENTO_TERRENO= 2;
		const ui PRECIO_INICIAL_TERRENO= 10;


		/* input de datos */
		const std::string BIENVENIDA_JUEGO = "Bienvenido a Granjeros Remasterized!";
		const std::string SOLICITUD_CANT_JUGADORES = "ingresa la cantidad de jugadores:";
		const std::string SOLICITUD_DIFICULTAD_JUEGO = "Ingresa una dificultad para el juego:";
		const std::string SOLICITUD_ANCHO_TERRENO = "Elija el ancho del terreno: ";
		const std::string SOLICITUD_ALTO_TERRENO = "Elija el alto del terreno: ";
		const std::string SOLICITUD_CANT_TURNOS = " ingrese la cantidad de turnos: ";

		const std::string ARCHIVO_CULTIVOS = "cultivos.txt";
		const std::string ARCHIVO_DESTINOS = "envios.txt";

		/* Nombres de cultivos */

		const std::string FRUTILLA = "frutilla";
		const std::string PAPA = "papa";
		const std::string LECHUGA = "lechuga";
		const std::string KIWI = "kiwi";

	/*Constantes utilizadas sobre las acciones de juego*/
		const ui COF_AMPLIACION_CAPACIDAD_TANQUE = 2;
		const ui COF_AMPLIACION_CAPACIDAD_ALMACEN = 2;
		const ui COF_VENTA_TERRENO= 2;



#endif /* CONSTANTESYTIPOS_H_ */
