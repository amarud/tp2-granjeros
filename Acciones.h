/*
 * Acciones.h
 *
 *  Created on: 25/5/2018
 *      Author: patron
 */

#ifndef ACCIONES_H_
#define ACCIONES_H_

#include "CondicionesIniciales.h"
#include "Almacen.h"
#include "Usuario.h"

typedef enum {
	SEMBRAR,
	COSECHAR,
	REGAR,
	ENVIAR,
	COMPRAR_TERRENO,
	VENDER_TERRENO,
	AUMENTAR_CAP_TANQUE_DE_AGUA,
	AUMENTAR_CAP_ALMACEN,
	FINALIZAR_TURNO
} acciones_t ;

class Acciones{

	private:

		acciones_t accionAct;



		/*
		 * Pre: recibe un apuntador a una lista de terrenos
		 * Post: devuelve un apuntador a una clase terreno.
		 */
		Terreno* obtenerTerreno(Lista<Terreno*>* listaDeTerrenos);


		/*Pre: recibe un apuntador a una lista de terrenos
		 *Post: devuelve la posicion del terreno válida en el rango [1..listaDeTerrenos->contarElementos()]
		 */
		ui validadorPosicionTerreno(Lista<Terreno*>* listaDeTerrenos);

		/*Pre: recibe un apuntador a una clase Almacen y una letra
		 *Post: devuelve un booleano: sera falso o verdadero dependiendo si se almaceno.
		 */
		bool almacenarCosecha(Almacen* almacen, Cultivo* nombreDeCultivo);

		/*Pre: recibe un apuntador a una clase terreno
		 *Post: devuelve la fila válida en el rango[1..terreno->verAlto()]
		 */
		ui validarFila(Terreno* terreno);

		/*Pre: recibe un apuntador a una clase terreno
		 *Post: devuelve la columna valida en el rango [1..terreno->verAncho()]
		 */
		ui validarColumna(Terreno* terreno);

		void actualizarParcelas(Terreno* terreno);

		void actualizarEstadoTerrenos(Usuario* jugador);

		void ajustarTiempoTerreno(Parcela* terreno);

		void actualizarCapacidadTanque(Usuario* jugador);

		void validar_stock_almacen(Usuario* jugador);

		ui buscarPosCultivo(Lista<Cultivo*>* cultivos, std::string nomCultivo);

		bool cultivoValido(Cultivo* cultivo, std::string nomCultivo);

		void mostrarCultivosDisponibles();

		bool numCultivoValido(ui numCultivo);

		std::string validarNumCultivo();

		std::string darCultivo(ui numDeCultivo);

	public:


		/*
		 * Post: Devuelve la accion Actual en el momento de la consulta.
		 */
		acciones_t verAccion();


		/*
		 * Pre: recibe un entero en el rango [1..MAX_ACCION_DISPONIBLE]
		 * Post: Reestablece la accion solicitada por el usuario.
		 * 	Preparandola para su Posterior ejecucion.
		 */
		void cambioDeAccion(ui numPedido);

		/*
		 * Pre: Jugador fue creado.
		 * Post: Ejecuta la acción solicitada por el usuario.
		 */
		void ejecutarAccion(Usuario* jugador, CondicionesIniciales* condicionesIniciales);

		/*
		 * Pre: almacen y lista de terrenos fueron Creados.
		 * Post: Devuelve un booleano: true si logro cosechar , false en caso contrario
		 */
		bool cosechar(Usuario* jugador, CondicionesIniciales* condIniciales);

		bool sembrar(Usuario* jugador, CondicionesIniciales* condIniciales);

		bool regar(Usuario* jugador);
		
		bool aumentarCapTanqueAgua(Usuario* jugador, CondicionesIniciales* condIniciales);
		
		bool aumentarCapAlmacen(Usuario* jugador, CondicionesIniciales* condIniciales);

		/* incrementa en 1, la cantidad de terrenos que posee jugador.
		 * Pre: jugador y condiciones iniciales fue creado.
		 * Post: Devuelve un valor de verdad sobre si logró comprar el terreno(true) ó no(false);
		 */
		bool comprarTerreno(Usuario* jugador, CondicionesIniciales* condIniciales);

		/* Reduce en 1, la cantidad de terrenos que posee el jugador,
		 * incrementa los creditos del jugador según la venta realizada.
		 * Pre: jugador y condIniciales fueron creados.
		 * Post: Devuelve True si se logró vender satisfactoriamete,
		 *       False en caso contrario.
		 */
		bool venderTerreno(Usuario* jugador, CondicionesIniciales* condIniciales);

		// Pre: el precio de envio debera ser mayor al de rentabilidad del cultivo
		bool enviar(Usuario* jugador, CondicionesIniciales* condIniciales);

		/* Modifica el estado de las parcelas según las reglas del juego propuestas.
		 * Luego de aplicarse, las parcelas ocupadas vuelven a encontrarse sin regar.
		 * Pre: jugador y condIniciales fueron creados.
		 * Post: Devuelve True si logró finalizar turno, False en caso contrario.
		 */
		bool finalizarTurno(Usuario* jugador, CondicionesIniciales* condIniciales);
};

#endif /* ACCIONES_H_ */
