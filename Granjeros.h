/*
 * Granjeros.h
 *
 *  Created on: 21/5/2018
 *      Author: brian
 */

#ifndef GRANJEROS_H_
#define GRANJEROS_H_

#include "CondicionesIniciales.h"
#include "Lista.h"
#include "Acciones.h"
#include "Usuario.h"

class Granjeros{
	private:
		Lista<Usuario*>* jugadores;
		Acciones* accionDisponible;
		CondicionesIniciales* parametrosIniciales;
		ui turnos;
		ui dificultad; // 1.FACIL 2.MEDIO 3.DIFICIL
		ui cantJugadores;
		ui ancho;
		ui alto;

	public:
		/* Constructor Granjeros */
		Granjeros();

		/* Destructor Granjeros */
		~Granjeros();

		/* Prepara el juego segun las condiciones que elijan los jugadores y las normas que establece el juego
		 *
		 * Post: Devuelve el/los jugador/es con sus datos inicializados*/
		void prepararJuego();

		/* Metodo donde se desarrolla el juego y donde interactuan los jugadores
		 *
		 */
		void jugarGranjeros();

		/* Se muestran los resultados finales de la partida
		 *
		 * Post: Muestra los creditos obtenidos al final de la partida por cada jugador
		 * y luego muestra por pantalla el ganador del torneo ó bien los jugadores que salieron empatados.
		 */
		void mostrarResultadosPartida();


		/*
		 *
		 */
		ui obtenerCantJugadores();

		/*
		 * Post: Devuelve la cantidad de turnos disponbles durante el juego.
		 */
		ui obtenerCantidadTurnos();

		/*
		 *
		 */
		Lista<Usuario*>* obtenerJugadores();

		Acciones* obtenerAccion();

		ui solicitarAccion();

		void mostrarEstadoJugadores();

	private:

		/* Prepara una lista de jugadores
		 *
		 * Post: Devuelve la lista de jugadores */
		void prepararJugadores();

		/* Prepara a cada jugador con las condiciones iniciales
		 *
		 * Post: Modifica las variables con que cuenta el jugador inicialmente y se le asigna un numero asociado*/
		void prepararJugador( Usuario* jugador, ui numJugador);

		/* Lee la cantidad de jugadores ingresada por el usuario
		 *
		 * Post: Devuelve la cantidad de jugadores */
		ui leerCantJugadores();

		/* Valida la cantidad de jugadores ingresados
		 *
		 * Post: Devuelve Verdadero si la cantidad es menor o igual a 4 y mayor que 0, caso contrario Falso */
		bool cantJugadoresValida( int cantJugadoresValida);

		/* Muestra las dificultades del juego a elegir
		 *
		 */
		void mostrarDificultadJuego();

		/*
		 * Post: mostro por pantalla todas las acciones de juego disponibles por el usuario en cada turno.
		 */
		void mostrarAccionesJuego();

		/*
		 * Post: Muestra los creditos de los distitos jugadores que participaron del juego.
		 * Indicando el numero de jugador respectivo.
		 */
		void mostrarRankingPorJugador();

		/*
		 * Busca el jugador con mayor cantidad de creditos en el rango de la lista [1..ultimaPosicion]
		 * Post: Devuelve la posicion donde se encuentra el jugador elegido.
		 */
		ui buscarMax(Lista<Usuario*>* jugadores,ui ultimaPosicion);

		/*
		 * Post: ordena la lista de jugadores en orden descendente segun
		 *  la cantidad de creditos obtenida por cada usuario.
		 */
		void ordenarJugadoresPorCreditos();

		/* lee la dificultad del juego
		 *
		 * Post: devuelve la dificultad del juego */
		ui leerDificultad();

		/* Valida la dificultad ingresada
		 *
		 * Post: Devuelve Verdadero si la dificultad esta en el rango valido, caso contrario Falso */
		bool dificultadValida( float dificultad);

		ui leerTurnos();

		void printfTerrenos(Usuario* jugadorAct);
		
		ui pedirAlto();
		ui pedirAncho();
};




#endif /* GRANJEROS_H_ */
