/*
 * ALmacen.cpp
 *
 *  Created on: 21 may. 2018
 *      Author: patron
 */

#include "Almacen.h"

Almacen::Almacen() {
	this->capacidad = 0;
	this->cosechas= new Lista<Cultivo*>;
}

ui Almacen::verCapacidad(){
	return this->capacidad;
}

bool Almacen::hayCapacidad(){
	return (verCapacidad() - verCantidadDeCultivos()) >= 0;
}

void Almacen::guardarCultivo(Cultivo* tipoDeCultivo){
	this->cosechas->agregar(tipoDeCultivo);
}

Lista<Cultivo*>* Almacen::verCosechas(){
	return this->cosechas;
}

void Almacen::setCapacidad(ui capacidad){
	this->capacidad = capacidad;
}

ui Almacen::verCantidadDeCultivos(){
	return this->cosechas->contarElementos();
}

Almacen::~Almacen(){
	delete this->cosechas;
}
