/*
 * Almacen.h
 *
 *  Created on: 21/05/2018
 *      Author: patron
 */

#ifndef ALMACEN_H_
#define ALMACEN_H_

#include "CondicionesIniciales.h"
#include "Lista.h"
#include "Cultivo.h"

/*Encargado de albergar los diferentes cultivos que vaya recolectando el
 * jugador a lo largo del juego, para su posterior envío.
 */
class Almacen {

private:

	ui capacidad;
	
	Lista<Cultivo*>* cosechas;
	

public:

	/*Constructor sin parámetros.
	 * Post: capacidad inicial es nula, Mientras que la lista se encuentra vacia.
	 */
	Almacen();

	/*
	 * Post: devuelve la capacidad de cosechas del almacen.
	 */
	ui verCapacidad();
	
	/*
	 * Post: cambia la cantidad de cosechas posibles de almacenar segun nuevaCapacidad.
	 */
	void setCapacidad(ui nuevaCapacidad);

	/* Pre: cultivo fue creado.
	 * Post: Se agrego un cultivo a las cosechas.
	 */
	void guardarCultivo (Cultivo* cultivo);

	/*
	 * Post: Devuelve las cosechas almacenadas hasta el momento.
	 */
	Lista<Cultivo*>* verCosechas();
	
	/*
	 * Post: Devuelve la cantidad de cosechas disponibles para su posterior envio.
	 */
	ui verCantidadDeCultivos();
	
	/*
	 * Post: Devuelve True, si la cantidad de cultivos cosechados no supera la capacidad del almacen
	 * False, en caso contrario.
	 */
	bool hayCapacidad();

	/* Destructor.
	 * Post: libera la memoria solicitada.
	 */
	~Almacen();

};

#endif /* ALMACEN_H_ */
