/*
 * CondicionesIniciales.h
 *
 *  Created on: 13/05/2018
 *      Author: patron
 */

#ifndef CONDICIONESINICIALES_H_
#define CONDICIONESINICIALES_H_

#include <string>
#include "ConstantesyTipos.h"
#include "Cultivo.h"

#include "Envio.h"
#include "Almacen.h"
#include "Lista.h"

//luego borrar
#include <iostream>


/* Esta clase se encagara de Procesar y Administrar las condiciones Iniciales del juego
 * Que seran constantes durante el mismo, a partir de los  parametros iniciales que ingrese
 * el usuario al comenzar el Juego.
 *
 * Todos los jugadores comenzaran con las mismas condiciones de juego por igual.
 */
typedef class CondicionesIniciales{
	private:
		Lista<Cultivo*>* cultivos;
		Lista<Envio*>* destinos;
		ui creditos;
		float dificultadReduccion;
		float dificultadAmpliacion;
		ui capacidadTanque;
		ui capacidadAlmacen;
		ui alto;
		ui ancho;
		

		/* Metodos que son utilizados al preparar las Condiciones Iniciales
		 * segun los datos ingresados por el usuario.
		 */
		ui calcularCapTanque(ui alto, ui ancho, float  dificultadReduccion);

		ui calcularCreditos(ui alto, ui ancho, float dificultadReduccion);
		
		ui calcularCapAlmacen(ui alto, ui ancho, float dificultadReduccion);
		
		/* Metodos internos que se utilizan para liberar correctamente la memoria
		 * de Condiciones Iniciales.
		 */
		void liberarEnvios();

		void liberarCultivos();

	public:



		/* Constructor sin parametros.
		 *
		 * Post: Inicializa todos los valores en cero e inicializa las listas
		 * 		de cultivos y envios, quedando inicialmente vacias.
		 */
		CondicionesIniciales();

		/* Pre: Alto y Ancho son dos enteros positivos.
		 * 		dificultad, tambien.
		 * 		(se considera a los parámetros como ya correctamente validados.)
		 *
		 * Post: Inicializa los valores iniciales del juego
		 * 		a partir los parametros ingresados por el usuario.
		 *
		 */
		void prepararParametrosIniciales(ui dificultad, ui ancho, ui alto);


		/* *****************************************
		 * Se encarga de cargar los Cultivos y Envios dados un par de archivos de los mismos
		 * correctamente formateados.
		 * Pre: Los Archivos son de la forma:
		 *
		 ******************************************
		 * Envios: archivo_envios.txt
		 *
		 * NOMBRE,COSTO,TIEMPO_DE_CRECIMIENTO,TIEMPO_DE_RECUPERACION, CONSUMO_DE_AGUA
		 *
		 * EJEMPLO : A,1,2,10,1,3
		 *
		 ******************************************
		 * Cultivos: archivo_cultivos.txt
		 *
		 * NOMBRE_DEL_DESTINO,DISTANCIA_EN_KM,PRECIO,CULTIVO
		 *
		 * EJEMPLO: SAN LUIS, 604, 20, A
		 *
		 * Post: Carga cada uno de los archivos, (Envios y Cultivos)
		 * 		respectivamente en cada lista asociada.
		 ******************************************
		 */
		void cargarArchivos();

		/*
		 * Post: Devuelve La cantidad de creditos iniciales.
		 */
		ui obtenerCreditos();

		/*
		 * Post: Devuelve la capacidad del tanque inicial.
		 * 	   Donde podra almacenar una determinada cantidad de unidades de riego
		 * 	   al finalizar el turno del jugador.
		 */
		ui obtenerCapacidadTanque();

		/*
		 * Pre: A mayor cantidad de terrenos, mayor es el valor calculado.
		 * Post: Devuelve el valor de la compra de un nuevo terreno.
		 * Segun la cantidad de terrenos que posee el jugador.
		 */
		ui calcularValorCompraTerreno(ui cantTerrenosJugador);

		/*
		 * Post: Devuelve el costo asociado a incrementar la capacidad del almacen.
		 * El costo es mayor a mayor dificultad elegida.
		 */
		ui calcularValorAumentarCapacidadAlmacen();

		/*
		 * Post: Devuelve el costo asociado a incrementar la capacidad del Tanque.
		 * El costo es mayor a mayor dificultad elegida.
		 */
		ui calcularValorAumentarCapacidadTanque();


		/*
		 * Post: Devuelve el ancho del terreno inicial.
		 */
		ui obtenerAnchoTerreno();

		/*
		 * Post: Devuelve el alto del terreno inicial.
		 */
		ui obtenerAltoTerreno();
		

		/*
		 * Post: Devuelve la capacidad del almacen, Donde
		 *  podra almacenar los distintos cultivos que se cosechen durante el juego
		 */
		ui obtenerCapacidadAlmacen();

		/* El coeficiente de ampliación es utilizado para calcular valores que
		 * crecen a medida que la dificultad aumenta.
		 *
		 * Post: Devuelve el coeficiente de dificultad de ampliacion,
		 * segun la dificultad elegida por el usuario.
		 *
		 * obteniendo:
		 *
		 * 1.00: Facil.
		 * 1.50: Medio.
		 * 2.00: Dificl.
		 */
		float obtenerDificultadEnAmpliacion();

		/* El coeficiente de reducción es utilizado para calcular valores que
		 * decrecen a medida que la dificultad aumenta.
		 *
		 * Post: Devuelve el coeficiente de dificultad de reduccion,
		 * segun la dificultad elegida por el usuario.
		 *
		 * obteniendo:
		 *
		 * 1.00: Facil.
		 * 0.75: Medio.
		 * 0.50: Dificl.
		 */
		float obtenerDificultadEnReduccion();
		
		/*
		 * Post: Devuelve la lista de Envios disponibles a realizar por el jugador.
		 */
		Lista<Envio*>* verDestinos();
		
		/*
		 * Post: Devuelve la lista de cultivos disponibles durante el juego.
		 */
		Lista<Cultivo*>* verCultivos();

		/* Obs:El cálculo se hace en base al valor del último terreno comprado,
	     *     Esto evita que el jugador pueda obtener ganacia a traves de la venta.
		 *
		 * Post: Devuelve el valor de la venta del ultimo terreno comprado.
		 */
		ui calcularValorVentaTerreno(ui cantTerrenos);


		~CondicionesIniciales();

}condIniciales_t;



#endif /* CONDICIONESINICIALES_H_ */
