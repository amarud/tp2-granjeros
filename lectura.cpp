
#include "lectura.h"
#include "Envio.h"
#include "Cultivo.h"
#include "Lista.h"

#include <fstream>
#include <iostream>
#include <string>

void cargarCultivos(std::string rutaEntrada, Lista<Cultivo*>* cultivos) {

	/* crea el archivo y abre la ruta especificada */
	std::ifstream entrada;
	entrada.open(rutaEntrada.c_str());

	if(entrada.is_open()){
			while(!entrada.eof()){
				char delimiter;
				std::string nomCultivo;
				unsigned int costoSem, tCrecimiento, rentabilidad;
				unsigned int tRecup, consumoAgua;

				//Intento de primer lectura.
				entrada >> nomCultivo;

				if(!entrada.eof()){
					entrada >> delimiter >> costoSem >> delimiter >> tCrecimiento >> delimiter;
					entrada >> rentabilidad >> delimiter >> tRecup >> delimiter >> consumoAgua;

					//Creo las clases correspondiendtes y agrego a lista en memoria dinámica.
					Cultivo* cultivoAct = new Cultivo( nomCultivo, costoSem, tCrecimiento, rentabilidad, tRecup, consumoAgua);
					cultivos->agregar(cultivoAct);
				}
			}

			/*cierra el archivo, liberando el recurso */
			entrada.close();
	}
	else{
		std::cout << "el archivo no fue correctamente abierto" << std::endl;
	}
}

void cargarEnvios(std::string rutaArchivo, Lista<Envio*>* envios) {

	/* crea el archivo y abre la ruta especificada */
	std::ifstream entrada;
	entrada.open(rutaArchivo.c_str());

	if(entrada.is_open()){
			while(!entrada.eof()){
				char delimiter;
				std::string nomCultivo;
				std::string nomDestino;
				unsigned int distancia, precio;

				entrada >> nomDestino;

				if(!entrada.eof()){
					entrada >> delimiter >> distancia >> delimiter;
					entrada >> precio >> delimiter >> nomCultivo;
					Envio* envioAct = new Envio(nomDestino, distancia, precio, nomCultivo);
					envios->agregar(envioAct);
				}

			}
			/*cierra el archivo, liberando el recurso */
			entrada.close();

	}
	else{
		std::cout << "el archivo no fue correctamente abierto" << std::endl;
	}
}


